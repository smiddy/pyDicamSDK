# pydicamsdk
A wrapper for the pco sensicam/dicam SDK

This class is a Python wrapper for the Sensicam SDK of pco AG, Germany
and provides an API to handle an pco camera in python.
It is tested and used with a Dicam Pro.

The class provides Pythonic usability. The `logging` class is implemented. Furthermore
a context object can be created for the secure closing of the camera in case of an error.

## Requirements
* pco Sensicam SDK
* Windows 64bit
* MS Visual C++ runtime
* Numpy

Tested with
* Win 10
* Anaconda 2018.12
* Python 3.6.8, 3.7.1
* Visual Build Tools 2017 installed (VS2015 packages included)

## Installation
For releases, Python wheels and eggs are provided (have a look in the repository)

From a command prompt with your Python environment, run

```python
python setup.py install
```
The error messaging system is realized as C Extension and will be compiled during installation.


## class ImgBuf
The class imgBuf is the object-oriented approach for the image buffers and aims to have buffer management within Python.

## class SensicamError
Exception Handle for sensicam class.

## class Sensicam
The constructor automatically intialized the PCI board and allocates
hdriver accordingly. The class sensicam is used for the control of the camera.

## Example
Examples are provided in `exampleDicma.py` and `exampleSensicam.py`.