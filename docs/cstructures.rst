Ctypes Structures
=================
Structures representing the C Structures in the SDK

.. autoclass:: dicamsdk.control.DPTimeLimitsDescription
    :members:
    :undoc-members:
    :show-inheritance:

.. autoclass:: dicamsdk.control.DicamProDesc
    :members:
    :undoc-members:
    :show-inheritance:

.. autoclass:: dicamsdk.control.CamParam
    :members:
    :undoc-members:
    :show-inheritance:

.. autoclass:: dicamsdk.control.CamValues
    :members:
    :undoc-members:
    :show-inheritance:
