Class Sensicam
================

.. autoclass:: dicamsdk.control.Sensicam
    :members:
    :undoc-members:
    :show-inheritance:
