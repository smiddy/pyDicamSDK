from setuptools import setup, Extension, find_packages
from distutils.util import convert_path

# Set the current version
main_ns = {}
ver_path = convert_path('dicamsdk/version.py')
with open(ver_path) as ver_file:
    exec(ver_file.read(), main_ns)
version = main_ns['__version__']

pcoErrorModule = Extension("dicamsdk.pcoError",
                           sources=["dicamsdk\pcoError.c"],
                           include_dirs=['C:\Program Files (x86)'
                                         '\Digital Camera Toolbox'
                                         '\Sensicam SDK\include'],
                           define_macros=[("PCO_ERRT_H_CREATE_OBJECT", None)],
                           )
setup(
    name="pydicamsdk",
    description="A wrapper for the pco sensicam/dicam SDK",
    version=version,
    platforms=["win-amd64", 'win32'],
    author="Markus J Schmidt",
    author_email='schmidt@ifd.mavt.ethz.ch',
    license="GNU GPLv3",
    url="https://github.com/smiddy/pydicamsdk",
    ext_modules=[pcoErrorModule],
    packages=find_packages(),
    install_requires=['numpy'],
    zip_safe=False,
    classifiers=['Development Status :: 4 - Beta',
                 'Environment :: Console',
                 'Intended Audience :: Science/Research',
                 'Intended Audience :: Education',
                 'Topic :: Scientific/Engineering',
                 'Operating System :: Microsoft :: Windows',
                 'License :: OSI Approved :: GNU General Public License v3 '
                 'or later (GPLv3+)',
                 'Programming Language :: Python :: 3.4',
                 'Topic :: Multimedia :: Graphics :: Capture :: Digital Camera',
                 'Topic :: Scientific/Engineering :: Visualization'
                 ]
)
